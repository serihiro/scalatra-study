package com.example.app

import org.json4s.{DefaultFormats, Formats}
import org.scalatra.ScalatraServlet
import org.scalatra.json.JacksonJsonSupport
import org.joda.time.{DateTime, DateTimeZone}
import java.security.MessageDigest

import scalaj.http._
import org.slf4j.LoggerFactory

import scala.util.Try
import scala.xml.{NodeSeq, XML}
import scalacache._
import scalacache.memcached.MemcachedCache
import concurrent.duration._

class SlidesController extends ScalatraServlet with JacksonJsonSupport {
  implicit val scalaCache = ScalaCache(MemcachedCache("localhost:11211"))

  override protected implicit def jsonFormats: Formats = DefaultFormats

  val logger = LoggerFactory.getLogger(getClass)
  val ApiKey = sys.env("SLIDESHARE_API_KEY")
  val ApiSecret = sys.env("SLIDESHARE_API_SECRET")
  val SearchSlideshowsUrl = "https://www.slideshare.net/api/2/search_slideshows"

  before() {
    contentType = formats("json")
  }

  get("/") {
    val query = Try(if (params("query").trim() != "") params("query").trim() else "").getOrElse("")
    val page = Try(if (params("page").trim().matches("^\\d$")) params("page").trim() else "1").getOrElse("1")

    fetchSlides(query, page)
  }

  private def fetchSlides(query: String, page: String): SlideList = {
    val cacheKey = query + "_" + page
    val cachedSlides = sync.get[SlideList, Array[Byte]](cacheKey).getOrElse(null)
    if (cachedSlides != null) {
      return cachedSlides
    }

    val timestamp = new DateTime(DateTimeZone.UTC).getMillis / 1000
    val md = MessageDigest.getInstance("SHA-1")
    md.update((ApiSecret + timestamp.toString()).getBytes)
    val hash = md.digest.foldLeft("") { (s, b) => s + "%02x".format(if (b < 0) b + 256 else b) }

    val parameters = Map(
      "api_key" -> ApiKey,
      "ts" -> timestamp.toString,
      "hash" -> hash,
      "q" -> query,
      "items_per_page" -> "20",
      "page" -> page,
      "lang" -> "ja",
      "sort" -> "latest"
    )

    // TODO Error handling
    val apiResponse: HttpResponse[String] = Http(SearchSlideshowsUrl).params(parameters).asString
    val rawSlidesXml = XML.loadString(apiResponse.body)
    val allSlides: NodeSeq = rawSlidesXml \ "Slideshow"
    val slides = allSlides.map(
      slide => new Slide(
        (slide \ "ID").text,
        (slide \ "Title").text,
        (slide \ "Description").text,
        (slide \ "Status").text,
        (slide \ "Username").text,
        (slide \ "URL").text,
        (slide \ "ThumbnailURL").text,
        (slide \ "ThumbnailSize").text,
        (slide \ "ThumbnailSmallURL").text,
        (slide \ "ThumbnailXLargeURL").text,
        (slide \ "ThumbnailXXLargeURL").text,
        (slide \ "Embed").text,
        (slide \ "Created").text,
        (slide \ "Updated").text,
        (slide \ "Language").text,
        (slide \ "Download").text,
        (slide \ "DownloadUrl").text,
        (slide \ "SlideshowType").text,
        (slide \ "InContest").text
      )
    ).toList

    val metas = rawSlidesXml \ "Meta"
    val resultOffset = Try((metas \ "ResultOffset").text.toInt).getOrElse(0)
    val numResults = Try((metas \ "NumResults").text.toInt).getOrElse(0)
    val totalResults = Try((metas \ "TotalResults").text.toInt).getOrElse(0)

    val slideList = new SlideList(1, false, false, 20, query, "newest", resultOffset, numResults, totalResults, slides, "tatuki")
    sync.cachingWithTTL[SlideList, Array[Byte]](cacheKey)(1.hour) {
      slideList
    }
    return slideList
  }
}

case class Slide(val id: String, val title: String, val description: String,
                 val status: String, val username: String, val url: String,
                 val thumbnailURL: String, val thumbnailSize: String,
                 val thumbnailSmallURL: String,
                 val thumbnailXLargeURL: String,
                 val thumbnailXXLargeURL: String,
                 val embed: String, val created: String, val updated: String,
                 val language: String, val download: String, val downloadUrl: String,
                 val slideshowType: String, val inContest: String)

case class SlideList(val currentPage: Int, val hasNext: Boolean, hasPrevious: Boolean,
                     requestPerItems: Int, query: String, sort: String,
                     resultOffset: Int, numResults: Int,
                     totalResults: Int, slides: List[Slide], cacheKey: String) {
}
